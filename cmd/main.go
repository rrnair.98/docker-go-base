package main

import (
	"database/sql"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/mysqldialect"
	"log"
	"net/http"
	"os"
	"strconv"
)

import (
	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	bun.BaseModel `bun:"table:users"`
	ID            int    `bun:"id,pk"`
	Name          string `bun:"name"`
}

var BunDb *bun.DB

func main() {
	dbHost := GetFromEnvOrDefault("DB_HOST", "0.0.0.0")
	user := GetFromEnvOrDefault("DB_USER", "root")
	password := GetFromEnvOrDefault("DB_PASSWORD", "Root@123")
	DBCon, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/hello_docker", user, password, dbHost))
	BunDb = bun.NewDB(DBCon, mysqldialect.New())
	if err != nil {
		panic(err)
	}

	defer BunDb.Close()
	router := gin.Default()

	router.GET("/users/:id", GetUser)

	router.Run("0.0.0.0:8080")

}

func GetFromEnvOrDefault(key string, def string) string {
	val, found := os.LookupEnv(key)
	if !found {
		return def
	}
	return val
}

func GetUser(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var user User
	log.Println("query: ", BunDb.NewSelect().Model(&user).Where("id = ?", id).String())
	err = BunDb.NewSelect().Model(&user).Where("id = ?", id).Scan(c)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Internal Server Error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, user)
}
